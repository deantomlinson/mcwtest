import Component from '@ember/component';
import { get } from '@ember/object'; 
import $ from 'jquery';

export default Component.extend({
	classNames: ['mdc-text-field'],

	didInsertElement: function () {
		let component = this;

		let componentJqueryObject = component.$();
		let componentElement = componentJqueryObject[0];

		let MDCTextField = mdc.textField.MDCTextField;     
		new MDCTextField(componentElement);
	}
});
